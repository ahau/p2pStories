import React, { Component } from 'react'
import { View, FlatList, Text, StyleSheet } from 'react-native'
import nodejs from 'nodejs-mobile-react-native'
import Audio from '../components/Audio'

export default class Feed extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isLoading: true,
      feed: []
    }

    this.reducer.bind(this)
    this.handleRefresh.bind(this)
  }

  handleRefresh = () => {
    this.setState(
      { isLoading: true },
      () => getFeed()
    )
  }

  componentDidMount () {
    this.listener = nodejs.channel.addListener('mutation', this.reducer, this)
    getFeed()
  }

  componentWillUnmount () {
    this.listener.remove() // solves setState on unmounted components!
  }

  reducer ({ type, payload }) {
    switch (type) {
      case 'feed':
        this.setState({
          isLoading: false,
          feed: payload
        })

        break

      default:
        //
    }
  }


  componentDidUpdate (prevProps, prevState) {
    if (
      prevProps.replicatedAt !== this.props.replicatedAt ||
      prevProps.feedUpdatedAt !== this.props.feedUpdatedAt
    ) {
      // Dirty hack to update
      getFeed()
    }
  }

  render () {
    if (this.state.isLoading) return Loading()

    return (
      <View style={{ flex: 1, width: '100%' }}>
        <FlatList
          refreshing={this.state.isLoading}
          onRefresh={this.handleRefresh}
          data={this.state.feed}
          renderItem={({ item }) => {
            const { author, authorName, content, timestamp } = item.value
            return (
              <Audio
                author={author}
                authorName={authorName}
                filePath={`http://localhost:26835/${content.blob}`}
                duration={content.duration}
                timestamp={timestamp}
              />
            )
          }}
          style={{ padding: 10 }}
          keyExtractor={(item, index) => item.key}
        />
      </View>
    )
  }
}

// const styles = StyleSheet.create({
// })

function Loading () {
  return <Text>Loading...</Text>
}

function getFeed () {
  dispatch({ type: 'getFeed' })
}

function dispatch (action) {
  // an asynchronous action somewhere else, probably involves db
  nodejs.channel.post('action', action)
}
